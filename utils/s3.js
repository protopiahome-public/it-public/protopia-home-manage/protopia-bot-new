import {
  gql,
} from '@apollo/client/core';

const getCircle = async (ctx) => {
  const circle = await ctx.graphql.query({
    context: { botContext: ctx },
    query: gql`
                query getCircleByTelegramId($telegram_id: String!) {
                    getCircleByTelegramId(telegram_id: $telegram_id) {
                        _id title
                    }
                }
            `,
    variables: {
      telegram_id: ctx.message.from.id.toString(),
    },
  });

  return circle.data.getCircleByTelegramId._id ? circle.data.getCircleByTelegramId : null;
};

export { getCircle };
