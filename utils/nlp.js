let Az = require('az');

const stringToWords = (str) => {
  const words = Az.Tokens(str).done()
    .filter((token) => token.type.toString() === 'WORD')
    .map((token) => token.source.substring(token.st, token.st + token.length));

  return words.map((word) => (Az.Morph(word).length ? Az.Morph(word)[0].normalize().toString() : word));
};

const compareStrings = (str1, str2) => JSON.stringify(stringToWords(str1)) === JSON.stringify(stringToWords(str2));

const wordNormalize = (word) => Az.Morph(word)[0].normalize().toString();

const stringToParts = (str) => Az.Tokens(str).done().map((token) => (
  token.source.substring(token.st, token.st + token.length)));

const stringToGroups = (str, groups) => {
  const parts = stringToParts(str);
  const result = [''];
  let currentGroup = 0;
  parts.forEach((part) => {
    if (part === groups[currentGroup]) {
      currentGroup++;
      result[currentGroup] = '';
    } else {
      result[currentGroup] += part;
    }
  });
  return result.map((item) => item.trim());
};

const sugarRuLocale = () => ({
  firstDayOfWeekYear: 1,
  units: 'миллисекунд:а|у|ы|,секунд:а|у|ы|,минут:а|у|ы|,час:||а|ов,день|день|дня|дней,недел:я|ю|и|ь|е,месяц:||а|ев|е,год|год|года|лет|году',
  months: 'янв:аря||.|арь,фев:раля||р.|раль,мар:та||т,апр:еля||.|ель,мая|май,июн:я||ь,июл:я||ь,авг:уста||.|уст,сен:тября||т.|тябрь,окт:ября||.|ябрь,ноя:бря||брь,дек:абря||.|абрь',
  weekdays: 'воскресенье|вс,понедельник|пн,вторник|вт,среда|ср,четверг|чт,пятница|пт,суббота|сб',
  numerals: 'ноль,од:ин|ну,дв:а|е,три,четыре,пять,шесть,семь,восемь,девять,десять',
  tokens: 'в|на,г\\.?(?:ода)?',
  short: '{dd}.{MM}.{yyyy}',
  medium: '{d} {month} {yyyy} г.',
  long: '{d} {month} {yyyy} г., {time}',
  full: '{weekday}, {d} {month} {yyyy} г., {time}',
  stamp: '{dow} {d} {mon} {yyyy} {time}',
  time: '{H}:{mm}',
  timeMarkers: 'в',
  ampm: ' утра, вечера',
  modifiers: [
    { name: 'day', src: 'позавчера', value: -2 },
    { name: 'day', src: 'вчера', value: -1 },
    { name: 'day', src: 'сегодня', value: 0 },
    { name: 'day', src: 'завтра', value: 1 },
    { name: 'day', src: 'послезавтра', value: 2 },
    { name: 'sign', src: 'назад', value: -1 },
    { name: 'sign', src: 'через', value: 1 },
    { name: 'shift', src: 'прошл:ый|ой|ом', value: -1 },
    { name: 'shift', src: 'следующ:ий|ей|ем', value: 1 },
  ],
  relative(num, unit, ms, format) {
    let numberWithUnit; let last = num.toString().slice(-1); let
      mult;
    switch (true) {
      case num >= 11 && num <= 15: mult = 3; break;
      case last == 1: mult = 1; break;
      case last >= 2 && last <= 4: mult = 2; break;
      default: mult = 3;
    }
    numberWithUnit = `${num} ${this.units[(mult * 8) + unit]}`;
    switch (format) {
      case 'duration': return numberWithUnit;
      case 'past': return `${numberWithUnit} назад`;
      case 'future': return `через ${numberWithUnit}`;
    }
  },
  parse: [
    '{num} {unit} {sign}',
    '{sign} {num} {unit}',
    '{months} {year?}',
    '{0?} {shift} {unit:5-7}',
  ],
  timeParse: [
    '{day|weekday}',
    '{0?} {shift} {weekday}',
    '{date} {months?} {year?} {1?}',
  ],
  timeFrontParse: [
    '{0?} {shift} {weekday}',
    '{date} {months?} {year?} {1?}',
  ],
});

export {
  stringToWords, compareStrings, wordNormalize, stringToParts, stringToGroups, sugarRuLocale,
};
