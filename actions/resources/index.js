import {
  gql,
} from '@apollo/client/core';
import { stringToWords, stringToParts, compareStrings } from '../../utils/nlp';
import { listWithPaging } from '../../utils/button';
import config from '../../config/config';

const showResources = async (ctx, page, edit) => {
  const resources = await ctx.graphql.query({
    context: { botContext: ctx },
    query: gql`
            query {
                getResources {
                    _id, title
                }
            }
        `,
  });

  listWithPaging({
    ctx,
    page,
    edit,
    perPage: 20,
    pagePrefix: 'resource-page',
    list: resources.data.getResources.map((resource) => `* ${resource.title}`),
  });
};

const addResource = async (ctx, text) => {
  const resource = await ctx.graphql.mutate({
    context: { botContext: ctx },
    mutation: gql`
        mutation changeResource($input:ResourceInput){
          changeResource(input: $input) {
              _id
            }
        }
    `,
    variables: {
      input: {
        title: text,
        description: text,
      },
    },
  });
  ctx.reply(`Ресурс добавлен. Список ресурсов: ${config.web_panel_url}/calendar/domain/resources\n\n`
  + `Отредактировать ресурс: ${config.web_panel_url}/calendar/domain/resources/${resource.data.changeResource._id}`);
};

const resourcesActions = (bot) => {
  bot.command('resources', async (ctx) => {
    showResources(ctx);
  });

  bot.on('text', async (ctx, next) => {
    if (compareStrings(ctx.message.text, 'бот ресурсы')) {
      showResources(ctx);
    } else {
      await next();
    }
  });

  bot.action(/^resource-page\|(.*)$/, async (ctx) => {
    await ctx.answerCbQuery();
    showResources(ctx, ctx.match[1], true);
  });

  bot.on('text', async (ctx, next) => {
    const words = stringToWords(ctx.message.text);
    if (compareStrings(words.slice(0, 3).join(' '), 'бот добавить ресурс')) {
      const text = stringToParts(ctx.message.text).slice(6).join('').trim();
      if (!text) {
        ctx.reply('Напишите описание ресурса.');
      } else {
        addResource(ctx, text);
      }
    } else {
      await next();
    }
  });
};

export default resourcesActions;
