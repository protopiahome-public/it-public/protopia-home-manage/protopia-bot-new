import { compareStrings } from '../../utils/nlp';

const help = async (ctx) => {
  const stickers = (await ctx.telegram.getStickerSet('ProtopiaDiscussion')).stickers;
  console.log(stickers);
  const sticker = stickers[Math.floor(Math.random() * stickers.length)];
  ctx.replyWithSticker(sticker.file_id);
};

const helpActions = (bot) => {
  bot.on('text', async (ctx, next) => {
    if (compareStrings(ctx.message.text, 'бот скажи что-нибудь умное') || compareStrings(ctx.message.text, 'бот помоги')) {
      help(ctx);
    } else {
      await next();
    }
  });

  bot.on('sticker', async (ctx, next) => {
    if (ctx.message.sticker.set_name === 'ProtopiaBotManage' && ctx.message.sticker.file_unique_id === 'AgADGwAD7PbvEQ') {
      help(ctx);
    } else {
      await next();
    }
  });
};

export default helpActions;
