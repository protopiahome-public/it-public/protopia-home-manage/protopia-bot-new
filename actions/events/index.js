import { google } from 'googleapis';
import sugar from 'sugar';
import moment from 'moment';
import { stringToWords, stringToGroups, sugarRuLocale } from '../../utils/nlp';
import configEvents from './config_events';

let CronJob = require('cron').CronJob;

sugar.Date.addLocale('ru', sugarRuLocale);

const auth = new google.auth.GoogleAuth({
  keyFile: `${__dirname}/../../config/google-service-token.json`,
  scopes: ['https://www.googleapis.com/auth/calendar'],
});

const addEvent = async (ctx, name, dateFrom, dateTo) => {
  const user = await ctx.getUser();
  if (!user || user.roles.includes('guest')) {
    return false;
  }
  if (!name || !dateFrom) {
    ctx.reply('Введите команду в виде\nБот событие дата начала [ - дата окончания] название Название события');
  }

  const calendar = google.calendar({ version: 'v3', auth });

  let dateToDateTime = dateTo ? sugar.Date.create(dateTo, 'ru') : sugar.Date.create(dateFrom, 'ru');
  if (!dateTo) {
    dateToDateTime.setHours(23, 59, 59);
  }

  const event = {
    summary: name,
    start: {
      dateTime: sugar.Date.create(dateFrom, 'ru').toISOString(),
    },
    end: {
      dateTime: dateToDateTime.toISOString(),
    },
  };

  calendar.events.insert({
    auth,
    calendarId: configEvents.calendarId,
    resource: event,
  }, (err, event) => {
    if (err) {
      console.log(`There was an error contacting the Calendar service: ${err}`);
      return;
    }
    ctx.reply(event.data.htmlLink);
  });
};

const notify = async (bot) => {
  const calendar = google.calendar({ version: 'v3', auth });

  let start = new Date();
  start.setHours(0, 0, 0, 0);
  let end = new Date();
  end.setHours(23, 59, 59, 999);

  const events = await calendar.events.list({
    auth,
    calendarId: configEvents.calendarId,
    timeMin: start,
    timeMax: end,
  });

  events.data.items.forEach((event) => {
    bot.telegram.sendMessage(configEvents.notifyChannel, `
${event.summary}
${event.description || ''}
${event.location || ''}
От ${moment(event.start.dateTime).format('DD.MM.YYYY HH:mm:ss')} до ${moment(event.end.dateTime).format('DD.MM.YYYY HH:mm:ss')}
`, {
  message_thread_id: configEvents.threadId,
});
  });
};

const eventsActions = (bot) => {
  bot.on('text', async (ctx, next) => {
    const words = stringToWords(ctx.message.text);
    if (words[0] === 'бот' && words[1] === 'событие') {
      const groups = stringToGroups(ctx.message.text, ['событие', 'название']);
      const dateGroups = stringToGroups(groups[1], ['-']);
      addEvent(ctx, groups[2], dateGroups[0], dateGroups[1]);
    } else {
      await next();
    }
  });

  if (configEvents.notifyEnabled) {
    let notifyJob = new CronJob('0 5 0 * * *', () => notify(bot));
    notifyJob.start();
  }
};

export default eventsActions;
