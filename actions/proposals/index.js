import {
  gql,
} from '@apollo/client/core';
import moment from 'moment';
import { buttonsWithPaging, buttons } from '../../utils/button';
import { compareStrings } from '../../utils/nlp';
import { getCircle } from '../../utils/s3';

const showProposals = async (ctx, edit, page) => {
  const proposals = await ctx.graphql.query({
    context: { botContext: ctx },
    query: gql`
            query {
                getProposals {
                    _id, title
                }
            }
        `,
  });
  buttonsWithPaging({
    ctx,
    edit,
    page,
    prefix: 'proposal',
    pagePrefix: 'proposal-page',
    message: 'Выберите предложение',
    buttons: proposals.data.getProposals.map((proposal) => ({ id: proposal._id, title: proposal.title })),
  });
};

const voteTitles = {
  yes: 'Отменяю возражение',
  no: 'Возражаю',
  doubt: 'Сомневаюсь',
};

const showProposal = async (ctx, id, edit) => {
  const proposalResult = await ctx.graphql.query({
    context: { botContext: ctx },
    query: gql`
            query getProposal($id: ID!){
                getProposal(id: $id) {
                  _id title date
                  author {name family_name telegram_id}
                  votes {date type author {_id name family_name telegram_id}}
                  external_entities {
                    id
                    type
                  }
                  circle {title}
              }
            }
        `,
    variables: { id },
  });
  const proposal = proposalResult.data.getProposal;

  let text = `<b>${proposal.title}</b>\n<i>`;
  text += proposal.author ? `<a href="tg://user?id=${proposal.author.telegram_id}">${proposal.author.name} ${proposal.author.family_name || ''}</a>` : null;
  text += `, ${moment(proposal.date).format('DD.MM.YYYY HH:mm:ss')}</i>`;
  if (proposal.circle) {
    text += `\nКруг: ${proposal.circle.title}`;
  }

  const timeout = new Date(proposal.date);
  timeout.setHours(timeout.getHours() + 2);

  const results = {};
  let result = 'yes';

  const votesText = proposal.votes ? proposal.votes.map((vote) => {
    if (vote.type === 'no') {
      timeout.setMinutes(timeout.getMinutes() + 30);
    }
    if (!results[vote.author._id]) {
      results[vote.author._id] = vote.type;
      if (vote.type === 'no') {
        result = 'no';
      }
    }
    return `----<a href="tg://user?id=${vote.author.telegram_id}">${vote.author.name} ${vote.author.family_name || ''}</a>: `
    + `${voteTitles[vote.type]} (${moment(vote.date).format('DD.MM.YYYY HH:mm:ss')})\n`;
  }).join('') : '';

  text += '\n\n';
  text += timeout < new Date()
    ? `Обсуждение заверешено в ${moment(timeout).format('DD.MM.YYYY HH:mm:ss')}. `
    : `Обсуждение продлится до ${moment(timeout).format('DD.MM.YYYY HH:mm:ss')}. Возразите, если считаете предложение вредным.`;
  text += timeout < new Date() ? ` <b>${result === 'yes' ? 'Принято' : 'Отменено'}</b>` : '';
  text += '\n\n';
  text += votesText;

  buttons({
    ctx,
    prefix: `proposal-answer|${id}`,
    edit,
    message: text,
    buttons: [
      [
        { id: 'no', title: 'Возражаю' },
        { id: 'doubt', title: 'Сомневаюсь' },
      ],
      [{ id: 'yes', title: 'Отменяю возражение' }],
      [
        { id: 'list', title: 'К списку' },
        { id: 'refresh', title: 'Обновить' },
      ],
    ],
  });
};

const vote = async (ctx, id, type) => {
  await ctx.graphql.mutate({
    context: { botContext: ctx },
    mutation: gql`
        mutation voteProposal($proposal_id:ID, $type:String){
            voteProposal(proposal_id: $proposal_id, type: $type) {
              _id
            }
        }
    `,
    variables: { proposal_id: id, type },
  });

  showProposal(ctx, id, true);
};

const addProposal = async (ctx, text) => {
  const circle = await getCircle(ctx);
  const proposal = await ctx.graphql.mutate({
    context: { botContext: ctx },
    mutation: gql`
        mutation changeMyProposal($input:ProposalInput){
            changeMyProposal(input: $input) {
              _id
            }
        }
    `,
    variables: {
      input: {
        title: text,
        circle_id: circle ? circle._id : null,
      },
    },
  });

  showProposal(ctx, proposal.data.changeMyProposal._id);
};

const proposalsActions = (bot) => {
  bot.command('proposals', async (ctx) => {
    showProposals(ctx);
  });

  bot.on('text', async (ctx, next) => {
    if (compareStrings(ctx.message.text, 'бот предложения')) {
      showProposals(ctx);
    } else {
      await next();
    }
  });

  bot.hears(/^\/add_proposal( ((.|\n)+))?$/m, async (ctx) => {
    if (!ctx.match[2]) {
      ctx.reply('Предложения добавляются командой\n/add_proposal Текст предложения');
    } else {
      addProposal(ctx, ctx.match[2]);
    }
  });

  bot.action(/^proposal-page\|(.*)$/, async (ctx) => {
    await ctx.answerCbQuery();
    showProposals(ctx, true, ctx.match[1]);
  });

  bot.action(/^proposal\|(.*)$/, async (ctx) => {
    await ctx.answerCbQuery();
    await showProposal(ctx, ctx.match[1], true);
  });

  bot.action(/^proposal-answer\|(.*)\|(.*)$/, async (ctx) => {
    await ctx.answerCbQuery();
    if (ctx.match[2] === 'list') {
      showProposals(ctx, true);
    } else if (ctx.match[2] === 'refresh') {
      showProposal(ctx, ctx.match[1], true);
    } else {
      vote(ctx, ctx.match[1], ctx.match[2]);
    }
  });
};

export default proposalsActions;
