import {
  gql,
} from '@apollo/client/core';
import config from '../../config/config';
import { buttonsWithPaging } from '../../utils/button';

const showGuests = async (ctx, edit, page) => {
  const users = await ctx.graphql.query({
    context: { botContext: ctx },
    query: gql`
            query {
                getUsers {
                    _id, name, family_name, roles
                }
            }
        `,
  });
  buttonsWithPaging({
    ctx,
    edit,
    page,
    prefix: 'user-activate',
    pagePrefix: 'user-activate-page',
    message: 'Выберите пользователя для активации',
    buttons: users.data.getUsers
      .filter((user) => user.roles && user.roles.includes('guest'))
      .map((user) => ({ id: user._id, title: `${user.name} ${user.family_name}` })),
  });
};

const activate = async (ctx, id) => {
  const user = await ctx.graphql.mutate({
    context: { botContext: ctx },
    mutation: gql`
        mutation changeUserRoles($_id:ID!, $roles:[String]!){
            changeUserRoles(_id: $_id, roles: $roles) {
              _id name family_name
            }
        }
    `,
    variables: { _id: id, roles: ['user'] },
  });

  ctx.reply(`${user.data.changeUserRoles.name} ${user.data.changeUserRoles.family_name} активирован!`);
};

const register = async (ctx) => {
  const user = await ctx.getUser();
  if (user) {
    return;
  }
  await ctx.graphql.mutate({
    context: { botContext: ctx },
    mutation: gql`
        mutation registerUser($input:UserInput){
          registerUser(input: $input) {
              _id
            }
        }
    `,
    variables: {
      input: {
        name: ctx.message.from.first_name,
        family_name: ctx.message.from.last_name,
        telegram_id: ctx.message.from.id,
      },
    },
  });
  ctx.reply('Вы зарегистрированы! Если вы в команде, вам скоро предоставят доступ.');
};

const panel = async (ctx) => {
  const user = await ctx.getUser();
  if (!user) {
    return;
  }
  if (ctx.message.from.id !== ctx.message.chat.id) {
    return;
  }
  const code = await ctx.graphql.mutate({
    context: { botContext: ctx },
    mutation: gql`
        mutation associate($input:AuthenticatorInput){
          associate(input: $input) {
                authenticator_type user_code
            }
        }
    `,
    variables: {
      input: {
        authenticator_type: 'otp',
        for_channel: 'web',
      },
    },
  });
  ctx.reply(`${config.web_panel_url}/login_external#${code.data.associate.user_code}`);
};

const circlesActions = (bot) => {
  bot.command('me', async (ctx) => {
    ctx.reply(JSON.stringify(await ctx.getUser(), null, 2));
  });

  bot.command('panel', async (ctx) => {
    panel(ctx);
  });

  bot.command('register', async (ctx) => {
    register(ctx);
  });

  bot.command('activate', async (ctx) => {
    showGuests(ctx);
  });

  bot.action(/^user-activate-page\|(.*)$/, async (ctx) => {
    await ctx.answerCbQuery();
    showGuests(ctx, true, ctx.match[1]);
  });

  bot.action(/^user-activate\|(.*)$/, async (ctx) => {
    await ctx.answerCbQuery();
    await activate(ctx, ctx.match[1]);
  });
};

export default circlesActions;
