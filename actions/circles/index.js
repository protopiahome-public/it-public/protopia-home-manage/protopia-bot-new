import {
  gql,
} from '@apollo/client/core';
import { compareStrings } from '../../utils/nlp';

const showCircles = async (ctx) => {
  const circles = await ctx.graphql.query({
    context: { botContext: ctx },
    query: gql`
            query {
                getCircles {
                    _id title telegram_id
                    members {
                      name family_name telegram_id
                    }
                }
            }
        `,
  });
  ctx.reply(circles.data.getCircles.map(
    (circle) => {
      let circleStr = circle.telegram_id ? `<a href="https://t.me/c/${circle.telegram_id.replace(/^-100/, '')}/100000000">${circle.title}</a>` : circle.title;
      if (parseInt(circle.telegram_id) === ctx.message.from.id) {
        circleStr = `<b>${circleStr}</b>`;
      }
      return `${circleStr}\n${
        circle.members.map((member) => {
          let memberStr = `${member.name || ''} ${member.family_name || ''}`;
          if (member.telegram_id) {
            memberStr = `<a href="tg://user?id=${member.telegram_id}">${memberStr}</a>`;
          }
          return `    ${memberStr}\n`;
        }).join('')}`;
    },
  ).join('\n'), { parse_mode: 'html' });
};

const circlesActions = (bot) => {
  bot.command('circles', async (ctx) => {
    showCircles(ctx);
  });

  bot.on('text', async (ctx, next) => {
    if (compareStrings(ctx.message.text, 'бот круги')) {
      showCircles(ctx);
    } else {
      await next();
    }
  });
};

export default circlesActions;
