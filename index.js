import { Telegraf } from 'telegraf';
import fs from 'fs';
import config from './config/config';
import userAuth, { clientAuth } from './pe/auth';

const Az = require('az');

const bot = new Telegraf(config.bot_token);

bot.use(userAuth);

fs.readdirSync(`${__dirname}/actions`, { withFileTypes: true })
  .forEach((dirent) => {
    if (dirent.isDirectory()) {
      require(`${__dirname}/actions/${dirent.name}/index.js`).default(bot);
    }
  });

bot.catch((err, ctx) => {
  console.error(err);
});

Az.Morph.init(() => {
  clientAuth().then(bot.launch());
});

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'));
process.once('SIGTERM', () => bot.stop('SIGTERM'));
